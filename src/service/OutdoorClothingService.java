package service;

import comparator.ProductComparatorByPrice;
import interfaces.Products;
import interfaces.Constants;
import model.Product;
import model.Clothing.OutdoorClothing;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class OutdoorClothingService implements Products, Constants {

    public ArrayList<Product> generateProducts(ArrayList<String> outdoorClothingsList) {

        ArrayList<Product> outdoorClothings = new ArrayList<>();
        for (String outdoorClothing : outdoorClothingsList) {
            outdoorClothings.add(new OutdoorClothing(outdoorClothing));
        }

        return outdoorClothings;
    }

    @Override
    public void addProduct() throws IOException {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter outdoor clothing name");
        String name = scanner.next();

        System.out.println("Enter outdoor clothing price");
        float price = scanner.nextFloat();

        System.out.println("Is outdoor clothing active product?");
        int isActive = scanner.nextInt();

        System.out.println("Enter outdoor clothing size");
        int size = scanner.nextInt();

        System.out.println("Enter outdoor clothing color");
        String color = scanner.next();

        System.out.println("Enter outdoor clothing season type");
        String seasonType = scanner.next();

        List<String> list = Arrays.asList(name, String.valueOf(price),
                String.valueOf(isActive), String.valueOf(size),
                color, seasonType);

        FileService.write(OUTDOOR_CLOTHING_PATH, String.join(",", list));
    }

    @Override
    public void printNames(ArrayList<Product> outdoorClothings) {
        for (Product outdoorClothing : outdoorClothings) {
            System.out.println("OutdoorClothing names : " + outdoorClothing.getName());
        }
    }

    @Override
    public Product cheapestProduct(ArrayList<Product> outdoorClothings) {
        Product cheapest = outdoorClothings.get(0);

        for (int i = 1; i < outdoorClothings.size(); i++) {
            if (outdoorClothings.get(i).getPrice() < cheapest.getPrice()) {
                cheapest = outdoorClothings.get(i);
            }
        }

        return cheapest;
    }

    @Override
    public void printCountOfNotActives(ArrayList<Product> outdoorClothings) {
        int count = 0;

        for (Product outdoorClothing : outdoorClothings) {
            if (!outdoorClothing.isActive())
                count++;
        }
        System.out.println("Count of not active outdoorClothings is " + count);
    }

    @Override
    public ArrayList<Product> orderByPrice(ArrayList<Product> outdoorClothings) {
        outdoorClothings.sort(new ProductComparatorByPrice());
        return outdoorClothings;
    }
}

package service;

import comparator.ProductComparatorByPrice;
import interfaces.Products;
import interfaces.Constants;
import model.Product;
import model.Food.Meat;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class MeatService implements Products, Constants {
    private String path = "data\\meats.txt";


    public ArrayList<Product> generateProducts(ArrayList<String> meatsList) {

        ArrayList<Product> meats = new ArrayList<>();
        for (String meat : meatsList) {
            meats.add(new Meat(meat));
        }

        return meats;
    }

    @Override
    public void addProduct() throws IOException {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter meat name");
        String name = scanner.next();

        System.out.println("Enter meat price");
        float price = scanner.nextFloat();

        System.out.println("Is meat active product?");
        int isActive = scanner.nextInt();

        System.out.println("Enter meat date");
        String date = scanner.next();

        System.out.println("Enter meat expired date");
        String expiredDate = scanner.next();

        System.out.println("Enter meat type");
        String meatType = scanner.next();

        List<String> list = Arrays.asList(name, String.valueOf(price),
                String.valueOf(isActive), date,
                expiredDate, meatType);

        FileService.write(MEAT_PATH, String.join(",", list));
    }

    @Override
    public void printNames(ArrayList<Product> meats) {
        for (Product meat : meats) {
            System.out.println("Meat names : " + meat.getName());
        }
    }

    @Override
    public Product cheapestProduct(ArrayList<Product> meats) {
        Product cheapest = meats.get(0);

        for (int i = 1; i < meats.size(); i++) {
            if (meats.get(i).getPrice() < cheapest.getPrice()) {
                cheapest = meats.get(i);
            }
        }

        return cheapest;
    }

    @Override
    public void printCountOfNotActives(ArrayList<Product> meats) {
        int count = 0;

        for (Product meat : meats) {
            if (!meat.isActive())
                count++;
        }
        System.out.println("Count of not active meats is " + count);
    }

    @Override
    public ArrayList<Product> orderByPrice(ArrayList<Product> meats) {
        meats.sort(new ProductComparatorByPrice());
        return meats;
    }
}

package service;

import comparator.ProductComparatorByPrice;
import interfaces.Products;
import interfaces.Constants;
import model.Product;
import model.Clothing.SportClothing;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class SportClothingService implements Products, Constants {

    public ArrayList<Product> generateProducts(ArrayList<String> sportClothingsList) {

        ArrayList<Product> sportClothings = new ArrayList<>();
        for (String sportClothing : sportClothingsList) {
            sportClothings.add(new SportClothing(sportClothing));
        }

        return sportClothings;
    }

    @Override
    public void addProduct() throws IOException {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter sport clothing name");
        String name = scanner.next();

        System.out.println("Enter sport clothing price");
        float price = scanner.nextFloat();

        System.out.println("Is sport clothing active product?");
        int isActive = scanner.nextInt();

        System.out.println("Enter sport clothing size");
        int size = scanner.nextInt();

        System.out.println("Enter sport clothing color");
        String color = scanner.next();

        System.out.println("Is sport clothing set?");
        int isSet = scanner.nextInt();

        List<String> list = Arrays.asList(name, String.valueOf(price),
                String.valueOf(isActive), String.valueOf(size),
                color, String.valueOf(isSet));

        FileService.write(SPORT_CLOTHING_PATH, String.join(",", list));
    }

    @Override
    public void printNames(ArrayList<Product> sportClothings) {
        for (Product sportClothing : sportClothings) {
            System.out.println("SportClothing names : " + sportClothing.getName());
        }
    }

    @Override
    public Product cheapestProduct(ArrayList<Product> sportClothings) {
        Product cheapest = sportClothings.get(0);

        for (int i = 1; i < sportClothings.size(); i++) {
            if (sportClothings.get(i).getPrice() < cheapest.getPrice()) {
                cheapest = sportClothings.get(i);
            }
        }

        return cheapest;
    }

    @Override
    public void printCountOfNotActives(ArrayList<Product> sportClothings) {
        int count = 0;

        for (Product sportClothing : sportClothings) {
            if (!sportClothing.isActive())
                count++;
        }
        System.out.println("Count of not active sportClothings is " + count);
    }

    @Override
    public ArrayList<Product> orderByPrice(ArrayList<Product> sportClothings) {
        sportClothings.sort(new ProductComparatorByPrice());
        return sportClothings;
    }
}

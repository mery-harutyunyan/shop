package service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;

public class FileService {
    public static void write(String path, String text) throws IOException {
        Files.write(Paths.get(path), text.getBytes(), StandardOpenOption.APPEND);
    }

    public static ArrayList<String> read(String path) throws IOException {
        return new ArrayList<>(Files.readAllLines(Paths.get(path)));
    }
}
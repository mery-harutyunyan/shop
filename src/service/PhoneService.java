package service;

import comparator.ProductComparatorByPrice;
import interfaces.Products;
import interfaces.Constants;
import model.Product;
import model.Electronics.Phone;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class PhoneService implements Products, Constants {

    public ArrayList<Product> generateProducts(ArrayList<String> phonesList) {

        ArrayList<Product> phones = new ArrayList<>();
        for (String phone : phonesList) {
            phones.add(new Phone(phone));
        }

        return phones;
    }

    @Override
    public void addProduct() throws IOException {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter phone name");
        String name = scanner.next();

        System.out.println("Enter phone price");
        float price = scanner.nextFloat();

        System.out.println("Is phone active product?");
        int isActive = scanner.nextInt();

        System.out.println("Enter phone voltage");
        float voltage = scanner.nextFloat();

        System.out.println("Enter phone power");
        double power = scanner.nextDouble();

        System.out.println("Enter phone OS");
        String OS = scanner.next();

        List<String> list = Arrays.asList(name, String.valueOf(price),
                String.valueOf(isActive), String.valueOf(voltage),
                String.valueOf(power), OS);

        FileService.write(PHONE_PATH, String.join(",", list));
    }

    @Override
    public void printNames(ArrayList<Product> phones) {
        for (Product phone : phones) {
            System.out.println("Phone names : " + phone.getName());
        }
    }

    @Override
    public Product cheapestProduct(ArrayList<Product> phones) {
        Product cheapest = phones.get(0);

        for (int i = 1; i < phones.size(); i++) {
            if (phones.get(i).getPrice() < cheapest.getPrice()) {
                cheapest = phones.get(i);
            }
        }

        return cheapest;
    }

    @Override
    public void printCountOfNotActives(ArrayList<Product> phones) {
        int count = 0;

        for (Product phone : phones) {
            if (!phone.isActive())
                count++;
        }
        System.out.println("Count of not active phones is " + count);
    }

    @Override
    public ArrayList<Product> orderByPrice(ArrayList<Product> phones) {
        phones.sort(new ProductComparatorByPrice());
        return phones;
    }
}

package service;

import interfaces.Constants;
import interfaces.Products;
import model.Product;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class MenuService implements Constants {
    public static final int OUTDOOR_CLOTHING_COMMANDS = 1;
    public static final int SPORT_CLOTHING_COMMANDS = 2;
    public static final int COMPUTER_COMMANDS = 3;
    public static final int PHONE_COMMANDS = 4;
    public static final int MEAT_COMMANDS = 5;
    public static final int VEGETABLE_COMMANDS = 6;
    public static final int EXIT_MENU = 7;

    public static final int ADD_PRODUCTS = 1;
    public static final int PRINT_PRODUCT_NAMES = 2;
    public static final int GET_CHEAPEST_PRODUCT = 3;
    public static final int PRINT_COUNT_NOT_ACTIVE_PRODUCTS = 4;
    public static final int ORDER_PRODUCTS_BY_PRICE = 5;
    public static final int EXIT_SUB_MENU = 6;

    public void generateMainMenu() throws IOException {
        boolean isMenuActive = true;

        while (isMenuActive) {
            Scanner scanner = new Scanner(System.in);
            printMenu();
            int commandNumber = scanner.nextInt();

            Products service = new OutdoorClothingService();
            ArrayList<String> dataList = new ArrayList<>();
            boolean isSubMenuActive = true;
            switch (commandNumber) {
                case OUTDOOR_CLOTHING_COMMANDS:
                    dataList = FileService.read(OUTDOOR_CLOTHING_PATH);
                    service = new OutdoorClothingService();
                    break;
                case SPORT_CLOTHING_COMMANDS:
                    dataList = FileService.read(SPORT_CLOTHING_PATH);
                    service = new SportClothingService();
                    break;
                case COMPUTER_COMMANDS:
                    dataList = FileService.read(COMPUTER_PATH);
                    service = new ComputerService();
                    break;
                case PHONE_COMMANDS:
                    dataList = FileService.read(PHONE_PATH);
                    service = new PhoneService();
                    break;
                case MEAT_COMMANDS:
                    dataList = FileService.read(MEAT_PATH);
                    service = new MeatService();
                    break;
                case VEGETABLE_COMMANDS:
                    dataList = FileService.read(VEGETABLE_PATH);
                    service = new VegetableService();
                    break;
                case EXIT_MENU:
                    isMenuActive = false;
                    isSubMenuActive = false;
                    System.out.println("Good bye");
                    break;
                default:
                    System.out.println("Invalid command number");
            }

            ArrayList<Product> products = service.generateProducts(dataList);
            while (isSubMenuActive) {
                printSubMenu();
                int subCommandNumber = scanner.nextInt();
                switch (subCommandNumber) {
                    case ADD_PRODUCTS:
                        service.addProduct();
                        break;
                    case PRINT_PRODUCT_NAMES:
                        service.printNames(products);
                        break;
                    case GET_CHEAPEST_PRODUCT:
                        System.out.println(service.cheapestProduct(products).toString());
                        break;
                    case PRINT_COUNT_NOT_ACTIVE_PRODUCTS:
                        service.printCountOfNotActives(products);
                        break;
                    case ORDER_PRODUCTS_BY_PRICE:
                        service.orderByPrice(products);
                        System.out.println(products.toString());
                        break;
                    case EXIT_SUB_MENU:
                        isSubMenuActive = false;
                        System.out.println("Exit from submenu");
                        break;
                    default:
                        System.out.println("Invalid command number");
                }
            }
        }
    }

    public void printMenu() {
        System.out.println("Select product type");
        System.out.println("1. Get Clothing : Outdoor clothing functionality");
        System.out.println("2. Get Clothing : Sport clothing functionality");
        System.out.println("3. Get Electronics : Computer functionality");
        System.out.println("4. Get Electronics : Phone functionality");
        System.out.println("5. Get Food : Meat functionality");
        System.out.println("6. Get Food : Vegetable functionality");
        System.out.println("7. Exit");
    }

    public void printSubMenu() {
        System.out.println("Enter command number");
        System.out.println("1. Add new product");
        System.out.println("2. Get product names");
        System.out.println("3. Get cheapest product");
        System.out.println("4. print count of not actives products");
        System.out.println("5. order products by price");
        System.out.println("6. Exit");
    }
}

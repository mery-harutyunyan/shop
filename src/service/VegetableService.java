package service;

import comparator.ProductComparatorByPrice;
import interfaces.Products;
import interfaces.Constants;
import model.Product;
import model.Food.Vegetable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class VegetableService implements Products, Constants {

    public ArrayList<Product> generateProducts(ArrayList<String> vegetablesList) {

        ArrayList<Product> vegetables = new ArrayList<>();
        for (String vegetable : vegetablesList) {
            vegetables.add(new Vegetable(vegetable));
        }

        return vegetables;
    }

    @Override
    public void addProduct() throws IOException {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter vegetable name");
        String name = scanner.next();

        System.out.println("Enter vegetable price");
        float price = scanner.nextFloat();

        System.out.println("Is vegetable active product?");
        int isActive = scanner.nextInt();

        System.out.println("Enter vegetable date");
        String date = scanner.next();

        System.out.println("Enter vegetable expired date");
        String expiredDate = scanner.next();

        System.out.println("Is vegetable GMO?");
        int isGMO = scanner.nextInt();

        List<String> list = Arrays.asList(name, String.valueOf(price),
                String.valueOf(isActive), date,
                expiredDate, String.valueOf(isGMO));

        FileService.write(VEGETABLE_PATH, String.join(",", list));
    }

    @Override
    public void printNames(ArrayList<Product> vegetables) {
        for (Product vegetable : vegetables) {
            System.out.println("Vegetable names : " + vegetable.getName());
        }
    }

    @Override
    public Product cheapestProduct(ArrayList<Product> vegetables) {
        Product cheapest = vegetables.get(0);

        for (int i = 1; i < vegetables.size(); i++) {
            if (vegetables.get(i).getPrice() < cheapest.getPrice()) {
                cheapest = vegetables.get(i);
            }
        }

        return cheapest;
    }

    @Override
    public void printCountOfNotActives(ArrayList<Product> vegetables) {
        int count = 0;

        for (Product vegetable : vegetables) {
            if (!vegetable.isActive())
                count++;
        }
        System.out.println("Count of not active vegetables is " + count);
    }

    @Override
    public ArrayList<Product> orderByPrice(ArrayList<Product> vegetables) {
        vegetables.sort(new ProductComparatorByPrice());
        return vegetables;
    }
}

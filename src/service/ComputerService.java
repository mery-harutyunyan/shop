package service;

import comparator.ProductComparatorByPrice;
import interfaces.Products;
import interfaces.Constants;
import model.Product;
import model.Electronics.Computer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class ComputerService implements Products, Constants {

    public ArrayList<Product> generateProducts(ArrayList<String> computersList) {
        ArrayList<Product> computers = new ArrayList<>();
        for (String computer : computersList) {
            computers.add(new Computer(computer));
        }

        return computers;
    }

    @Override
    public void addProduct() throws IOException {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter computer name");
        String name = scanner.next();

        System.out.println("Enter computer price");
        float price = scanner.nextFloat();

        System.out.println("Is computer active product?");
        int isActive = scanner.nextInt();

        System.out.println("Enter computer voltage");
        float voltage = scanner.nextFloat();

        System.out.println("Enter computer power");
        double power = scanner.nextDouble();

        System.out.println("Enter computer processor");
        String processor = scanner.next();

        System.out.println("Enter computer memory");
        int memory = scanner.nextInt();

        List<String> list = Arrays.asList(name, String.valueOf(price),
                String.valueOf(isActive), String.valueOf(voltage),
                String.valueOf(power), processor, String.valueOf(memory));

        FileService.write(COMPUTER_PATH, String.join(",", list));
    }

    @Override
    public void printNames(ArrayList<Product> computers) {
        for (Product computer : computers) {
            System.out.println("Computer names : " + computer.getName());
        }
    }

    @Override
    public Product cheapestProduct(ArrayList<Product> computers) {
        Product cheapest = computers.get(0);

        for (int i = 1; i < computers.size(); i++) {
            if (computers.get(i).getPrice() < cheapest.getPrice()) {
                cheapest = computers.get(i);
            }
        }

        return cheapest;
    }

    @Override
    public void printCountOfNotActives(ArrayList<Product> computers) {
        int count = 0;

        for (Product computer : computers) {
            if (!computer.isActive())
                count++;
        }
        System.out.println("Count of not active computers is " + count);
    }

    @Override
    public ArrayList<Product> orderByPrice(ArrayList<Product> computers) {
        computers.sort(new ProductComparatorByPrice());
        return computers;
    }
}

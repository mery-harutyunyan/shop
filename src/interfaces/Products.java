package interfaces;


import model.Product;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Product interface
 */
public interface Products {

    ArrayList<Product> generateProducts(ArrayList<String> dataList);

    void addProduct() throws IOException;

    void printNames(ArrayList<Product> products);

    Product cheapestProduct(ArrayList<Product> products);

    void printCountOfNotActives(ArrayList<Product> products);

    ArrayList<Product> orderByPrice(ArrayList<Product> computers);
}

package interfaces;

public interface Constants {

    public static final String OUTDOOR_CLOTHING_PATH = "data\\outdoor_clothings.txt";
    public static final String SPORT_CLOTHING_PATH = "data\\sport_clothings.txt";
    public static final String COMPUTER_PATH = "data\\computers.txt";
    public static final String PHONE_PATH = "data\\phones.txt";
    public static final String MEAT_PATH = "data\\meats.txt";
    public static final String VEGETABLE_PATH = "data\\vegetables.txt";
}

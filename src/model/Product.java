package model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class Product {
    private String name;
    private String brand;
    private float price;
    private float discount;
    private int quantity;
    private boolean isActive;

    public abstract String getDescription();

    public abstract ArrayList<String> getFilters();

    public Product(String name, float price) {
        this.name = name;
        this.price = price;
    }

    public Product(String data) {
        String[] d = data.split(",");

        this.name = d[0];
        this.price = Float.parseFloat(d[1]);
        this.isActive = Boolean.parseBoolean(d[2]);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public float getDiscount() {
        return discount;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public void printInfo() {
        System.out.println("------------------------");
        System.out.println("Name : " + name);
        System.out.println("Price : " + price);
        System.out.println("Quantity : " + quantity);
        System.out.println("Brand : " + brand);
        System.out.println("------------------------");
    }

    public String toString() {
        List<String> list = Arrays.asList("{",
                "name : ", name, ",",
                "price : ", String.valueOf(price), ",",
                "is active : ", String.valueOf(isActive),
                "}"
        );

        return String.join(" ", list);
    }
}

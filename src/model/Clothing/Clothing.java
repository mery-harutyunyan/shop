package model.Clothing;

import model.Product;

public abstract class Clothing extends Product {
    private int size;
    private String color;

    public Clothing(String name, float price, int size, String color) {
        super(name, price);
        this.size = size;
        this.color = color;
    }

    public Clothing(String data) {
        super(data);
        String[] d = data.split(",");
        this.size = Integer.parseInt(d[3]);
        this.color = d[4];
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public void printInfo() {
        super.printInfo();
        System.out.printf("Product Size: %d %n", size);
        System.out.printf("Product Color: %s %n", color);
    }

    @Override
    public String getDescription() {
        StringBuffer str = new StringBuffer("Product name is ")
                .append(getName())
                .append("Product size is")
                .append(size)
                .append("Product color is")
                .append(color);

        return str.toString();
    }


}

package model.Clothing;

import java.util.ArrayList;

public class OutdoorClothing extends Clothing {
    private String seasonType;

    public OutdoorClothing(String name, float price, int size, String color, String seasonType) {
        super(name, price, size, color);
        this.seasonType = seasonType;
    }

    public OutdoorClothing(String data){
        super(data);
        String[] d = data.split(",");
        this.seasonType = d[4];
    }

    public String getSeasonType() {
        return seasonType;
    }

    public void setSeasonType(String seasonType) {
        this.seasonType = seasonType;
    }

    @Override
    public ArrayList<String> getFilters() {
        ArrayList<String> filters = new ArrayList<>();
        filters.add("size");
        filters.add("color");
        filters.add("seasonType");

        return filters;
    }
}

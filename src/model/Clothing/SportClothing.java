package model.Clothing;

import java.util.ArrayList;

public class SportClothing extends Clothing {

    private boolean isSet;

    public SportClothing(String name, float price, int size, String color, boolean isSet) {
        super(name, price, size, color);
        this.isSet = isSet;
    }

    public SportClothing(String data){
        super(data);
        String[] d = data.split(",");
        this.isSet = Boolean.parseBoolean(d[5]);
    }

    public boolean isSet() {
        return isSet;
    }

    public void setSet(boolean set) {
        isSet = set;
    }

    @Override
    public ArrayList<String> getFilters() {
        ArrayList<String> filters = new ArrayList<>();
        filters.add("size");
        filters.add("color");
        filters.add("isSet");

        return filters;
    }
}

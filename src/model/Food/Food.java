package model.Food;

import model.Product;

public abstract class Food extends Product {
    private String date;
    private String expiredDate;

    public Food(String name, float price, String date, String expiredDate) {
        super(name, price);
        this.date = date;
        this.expiredDate = expiredDate;
    }

    public Food(String data){
        super(data);
        String[] d = data.split(",");
        this.date = d[3];
        this.expiredDate = d[4];
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(String expiredDate) {
        this.expiredDate = expiredDate;
    }

    @Override
    public void printInfo() {
        super.printInfo();
        System.out.printf("Product Date: %s %n", date);
        System.out.printf("Product Expired Date: %s %n", expiredDate);
    }

    @Override
    public String getDescription(){
        StringBuffer str = new StringBuffer("Product name is ")
                .append(getName())
                .append("Product date is")
                .append(date)
                .append("Product expired date is")
                .append(expiredDate);

        return str.toString();
    }

}

package model.Food;

import java.util.ArrayList;

public class Meat extends Food {
    private String meatType;

    public Meat(String name, float price, String date, String expiredDate, String meatType) {
        super(name, price, date, expiredDate);
        this.meatType = meatType;
    }
    public Meat(String data) {
        super(data);
        String[] d = data.split(",");
        this.meatType = d[5];
    }

    @Override
    public ArrayList<String> getFilters() {
        ArrayList<String> filters = new ArrayList<>();
        filters.add("date");
        filters.add("expiredDate");
        filters.add("meatType");

        return filters;
    }
}

package model.Food;

import java.util.ArrayList;

public class Vegetable extends Food {
    private boolean isGMO;

    public Vegetable(String name, float price, String date, String expiredDate, boolean isGMO) {
        super(name, price, date, expiredDate);
        this.isGMO = isGMO;
    }

    public Vegetable(String data){
        super(data);
        String[] d = data.split(",");
        this.isGMO = Boolean.parseBoolean(d[5]);
    }

    @Override
    public ArrayList<String> getFilters() {
        ArrayList<String> filters = new ArrayList<>();
        filters.add("date");
        filters.add("expiredDate");
        filters.add("isGMO");

        return filters;
    }
}

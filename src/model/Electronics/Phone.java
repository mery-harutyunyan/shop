package model.Electronics;

import java.util.ArrayList;

public class Phone extends Electronics {
    private String OS;

    public Phone(String name, float price, float voltage, double power, String OS) {
        super(name, price, voltage, power);
        this.OS = OS;
    }

    public Phone(String data){
        super(data);
        String[] d = data.split(",");
        this.OS = d[5];
    }

    public String getOS() {
        return OS;
    }

    public void setOS(String OS) {
        this.OS = OS;
    }

    @Override
    public ArrayList<String> getFilters() {
        ArrayList<String> filters = new ArrayList<>();
        filters.add("OS");

        return filters;
    }
}

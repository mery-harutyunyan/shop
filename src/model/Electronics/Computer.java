package model.Electronics;

import java.util.ArrayList;

public class Computer extends Electronics {
    private String processor;
    private int memory;

    public Computer(String name, float price, float voltage, double power, String processor, int memory) {
        super(name, price, voltage, power);
        this.processor = processor;
        this.memory = memory;
    }

    public Computer(String data) {
        super(data);
        String[] d = data.split(",");
        this.processor = d[5];
        this.memory = Integer.parseInt(d[6]);
    }

    public String getProcessor() {
        return processor;
    }

    public void setProcessor(String processor) {
        this.processor = processor;
    }

    public int getMemory() {
        return memory;
    }

    public void setMemory(int memory) {
        this.memory = memory;
    }

    @Override
    public ArrayList<String> getFilters() {
        ArrayList<String> filters = new ArrayList<>();
        filters.add("processor");
        filters.add("memory");

        return filters;
    }
}

package model.Electronics;


import model.Product;

import static java.lang.Double.parseDouble;
import static java.lang.Float.parseFloat;

public abstract class Electronics extends Product {
    private float voltage;
    private double power;

    public Electronics(String name, float price, float voltage, double power) {
        super(name, price);
        this.voltage = voltage;
        this.power = power;
    }

    public Electronics(String data){
        super(data);
        String[] d = data.split(",");
        this.voltage = parseFloat(d[3]);
        this.power = parseDouble(d[4]);

    }

    public float getVoltage() {
        return voltage;
    }

    public void setVoltage(float voltage) {
        this.voltage = voltage;
    }

    public double getPower() {
        return power;
    }

    public void setPower(double power) {
        this.power = power;
    }

    @Override
    public void printInfo() {
        super.printInfo();
        System.out.printf("Product Voltage: %.2f %n", voltage);
        System.out.printf("Product Power: %.2f %n", power);
    }

    @Override
    public String getDescription() {
        StringBuffer str = new StringBuffer("Product name is ")
                .append(getName())
                .append("Product voltage is")
                .append(voltage)
                .append("Product power is")
                .append(power);

        return str.toString();
    }
}
